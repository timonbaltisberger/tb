"""Transcript sampler.

Created on Thu Oct 14 23:12:48 2021
@author: timonbaltisberger
"""

from setuptools import setup, find_packages

from transcriptsampler import __version__

setup(
    name='transcriptsampler',
    url='https://gitlab.com/timonbaltisberger/tb.git',
    author='Timon Baltisberger',
    author_email='t.baltisberger@stud.unibas.ch',
    description=(
        'Returns a sampling of transcripts based on an expression frequency'
    ),
    license='MIT',
    version=__version__,
    packages=find_packages(),
    install_requires=[],
    entry_points={
        'console_scripts': ['transcript-sampler=transcriptsampler.cli:main'],
    },
)
