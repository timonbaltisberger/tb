# import pytest
from transcript_structure import Generate_transcript_structure as gts

TEST_CSV_TITLE = './resources/RIK_5_RP1_5_title.csv'
TEST_CSV_NO_TITLE = './resources/RIK_5_RP1_5_no_title.csv'

GENE_COORDS = './tests/resources/RP1_RIK.gtf'
GENE_KEYS = ['Rp1','1700034P13Rik']

P_INTRON_0: float = 0
P_INTRON_0_2 = 0.2
P_INTRON_1: float = 1

with open(TEST_CSV_TITLE) as csv:
    csv_lines = csv.readlines()
first_line = csv_lines[0].split(',')

#end