"""Runs tests."""

from pathlib import Path

import pytest

from transcriptsampler.transcript_sampler import TranscriptSampler as ts

TEST_FILE = "./tests/resources/example_genes.tsv"
TEST_INPUT = {
    "A": 1,
    "B": 2,
    "C": 3,
    "D": 4,
}


def test_read_avg_expression():
    """Tests for reading average expression."""
    sampler = ts(TEST_FILE)
    sampler.read_avg_expression()
    assert sampler.gene_expression == TEST_INPUT


@pytest.mark.parametrize(
        "test_input, expected",
        [((TEST_INPUT, 1), 1)],
)
def test_sample_transcripts(test_input, expected):
    """Tests for sampling transcripts."""
    sampler = ts(TEST_FILE)
    sampler.gene_expression = test_input[0]
    tot = 0
    sampler.sample_transcripts(
        num_transcripts=test_input[1],
    )
    for key in sampler.transcripts_per_gene.keys():
        tot += sampler.transcripts_per_gene[key]
    assert(tot == expected)


def test_write_sample(tmpdir):
    """Test for writing sampling output."""
    sampler = ts(TEST_FILE)
    sampler.transcripts_per_gene = TEST_INPUT
    file_name = Path(tmpdir / 'output.txt')
    assert file_name.is_file() == False
    sampler.write_sample(
        file_name=file_name,
    )
    assert file_name.is_file() == True
