import random
import csv
import copy


class BuildTranscriptStructure:

    """Creates differently spliced transcripts.

    Args:
        input_gene_count(str): Path to csv file of type "geneID", number to sample.
        input_coordinates(str): Path to gtf file of relevant genes.
        p_intron(float): Probability to include each intron in the mRNA sequence.

    Attributes:
        gene_count_dict(dict): Dictionary of format {"gene_ID": number_of_samplings}
        gene_sequences_dict(dict): Nested dictionary with information about each gene.
            Format: {"gene_ID": {"gene_line": gene_gtf_line,
                                 "transcript_line": transcript_gtf_line,
                                 "exon_line": exemplary_exon_line,
                                 "exon_seq": [[start_exon1, end_exon1],[start_exon2, end_exon2],...]
                                 }
                    }
        gene_transcript_dict(dict): Nested dictionary with amount of each differently spliced transcript.
            Format: {"gene_ID": {"transcript_ID": n_copies}}
            The transcript ID is a binary code, signifying whether a certain intron is included or not.
            E.g. gene with 4 exons:
                transcript code 001: The 1. and 2. introns are not included (spliced away), the 3. is included.
                The transcript will therefore have 3 exons (exon 3 and 4 are combined).
            For sequences with negative strand senses, the exon numbering is determining the direction the transcript
            code is to be applied, and not the occurrence in the gene sequence (inverted by convention).
            E.g. '01' means the intron between original (fully spliced) exon 1 and exon 2 was spliced away,
                        but the intron between exon 2 and exon 3 is included in the transcript.
        gtf_lines(list): List with all newly created gtf lines.
    """

    def __init__(self,
                 input_gene_count: str,
                 input_coordinates: str,
                 p_intron: float,
                 ) -> None:
        """Class constructor."""
        self.gene_count = input_gene_count
        self.input_coords = input_coordinates
        self.p_intron = p_intron
        self.gene_count_dict = {}
        self.gene_sequences_dict = {}
        self.gene_transcript_dict = {}
        self.gtf_lines = []

    def generate_transcript_structure(self):
        """Computes distribution and gene coordinates of differently spliced mRNA."""
        self.csv_2_dict()  # Generates dictionary from gene count csv file.
        self.gtf_2_dict()  # Generates dictionary from gtf file.
        self.make_new_transcripts()  # Generates the differently spliced transcripts.
        self.make_gtf_info()  # Builds the gtf file of all newly created transcripts.
        self.sort_gtf_lines()  # Sorts the gtf file by gene occurrence in sequence.

    def csv_2_dict(self) -> None:
        """Converts the csv file with gene count into a dictionary."""
        with open(self.gene_count) as g_c:
            lines = g_c.readlines()

        first_line = lines[0].split(',')  # Removes the first line if it is a title.
        if not first_line[1][0].isnumeric():
            del lines[0]

        for line in lines:
            line_entries = line.split(',')
            self.gene_count_dict[line_entries[0]] = int(line_entries[1])

    def gtf_2_dict(self) -> None:
        """Converts the gtf file into a nested dictionary."""
        with open(self.input_coords) as c_g:  # Reads coordinates from .gtf file.
            lines = c_g.readlines()
        lines = [i for i in lines if i[0] != '#']  # Exclude comments

        for gene_line in range(len(lines)):
            gene_info = {}  # Dictionary with information of a single gene.
            line_entries = lines[gene_line].split('\t')
            if line_entries[2] == 'gene':  # The line indeed describes a gene.
                attribute = line_entries[8].split(';')
                gene_name = attribute[2][12:-1]  # Extracts the gene name from the attributes.
                gene_info['gene_line'] = lines[gene_line]
                gene_info['transcript_line'] = lines[gene_line + 1]
                gene_info['exon_line'] = lines[gene_line + 2]  # Exemplary line of an exon.
                gene_info['strand_sense'] = line_entries[6] == '+'  # Strand sense.

                coordinates = []
                exon_line = []
                line_offset = 2  # Lines after the gene line (+1 is transcript description).
                while True:
                    try:  # Avoids error at end of list.
                        exon_line = lines[gene_line + line_offset].split('\t')
                    except IndexError:  # End of gtf file reached: The job is finished.
                        break
                    if exon_line[2] != 'exon':  # End of exon list of this gene is reached.
                        break
                    else:  # The line is an exon.
                        coordinates.append([int(exon_line[3]), int(exon_line[4])])
                    line_offset += 1  # Move to next line.

                if exon_line[6] == '-':  # Strands with sense (-)
                    coordinates.reverse()

                gene_info['exon_seq'] = coordinates
                self.gene_sequences_dict[gene_name] = gene_info

    def make_new_transcripts(self) -> None:
        """ Generates the differently spliced transcripts."""
        for gene in self.gene_count_dict:

            # Computes the intron splicing for each transcript.
            transcript_ids = []
            for _ in range(self.gene_count_dict[gene]):
                i_d = []
                for __ in range(len(self.gene_sequences_dict[gene]['exon_seq']) - 1):
                    if random.random() > self.p_intron:  # Intron spliced away.
                        i_d.append('0')
                    else:  # Intron not spliced away.
                        i_d.append('1')
                transcript_ids.append(''.join(i_d))  # Combine all transcript IDs in one list.

            # Counts how often each transcript is is the list.
            transcript_numbers = {}
            while True:
                i_d = transcript_ids.pop()
                transcript_numbers['-'.join([gene, i_d])] = 1 + transcript_ids.count(i_d)
                transcript_ids = [not_current_iD for not_current_iD in transcript_ids if not_current_iD != i_d]
                if not transcript_ids:  # Leaves loop once all codes were scanned for.
                    break

            self.gene_transcript_dict[gene] = transcript_numbers

    def write_csv(self,
                  output_transcript_count: str
                  ) -> None:

        """ Writes a csv file containing the number of differently spliced transcripts.

        Args:
             output_transcript_count(str): Path and name of the output cvs file: "transcript_ID", "gene_ID", count.
        """

        with open(output_transcript_count, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(['Transcript_ID', 'Gene_ID', 'count'])
            for gene in self.gene_transcript_dict:
                for transcript_ID in self.gene_transcript_dict[gene]:
                    writer.writerow([transcript_ID, gene, self.gene_transcript_dict[gene][transcript_ID]])

    def make_gtf_info(self) -> None:
        """ Writes the lines of the new gtf file for the differently spliced transcripts."""
        for gene in self.gene_transcript_dict:  # Iterates over all genes required.
            self.gtf_lines.append(self.gene_sequences_dict[gene]['gene_line'])  # Add gene line to list.
            sense = self.gene_sequences_dict[gene]['strand_sense']

            for transcript_ID in self.gene_transcript_dict[gene]:  # Iterates over all occurring types of splicings.
                # Modifies the transcript line according to the splicing.
                transcript_line = self.gene_sequences_dict[gene]['transcript_line'].split('\t')
                attribute = transcript_line[8].split(';')
                attribute[7] = ''.join(['transcript_name "', transcript_ID, '"'])
                transcript_line[8] = '; '.join(attribute)
                self.gtf_lines.append('\t'.join(transcript_line))

                start_id = len(gene)
                i_d = list(map(int, transcript_ID[start_id + 1:]))  # Extract the splicing coding as int.
                i_d_pop = copy.copy(i_d)
                if sense:
                    i_d_pop.reverse()
                numb_introns = sum(i_d)  # Adds up the transcription ID as int.
                numb_exons = len(self.gene_sequences_dict[gene]['exon_seq']) - numb_introns

                n_unspliced = 0  # Count of number of unspliced introns.
                exon_lines = []
                for exon in range(numb_exons):
                    exon_line = self.gene_sequences_dict[gene]['exon_line'].split('\t')  # Initializes exon line.
                    exon_line[2] = 'exon'
                    attribute = exon_line[8].split(';')
                    if sense:
                        attribute[4] = ''.join(['exon_number "', str(exon + 1), '"'])
                    else:
                        attribute[4] = ''.join(['exon_number "', str(numb_exons - exon), '"'])
                    attribute[8] = ''.join(['transcript_name "', transcript_ID, '"'])
                    exon_line[8] = '; '.join(attribute)
                    exon_line[3] = str(self.gene_sequences_dict[gene]['exon_seq'][exon + n_unspliced][0])
                    exon_line[4] = str(self.gene_sequences_dict[gene]['exon_seq'][exon + n_unspliced][1])

                    try:
                        while i_d_pop.pop():
                            n_unspliced += 1
                            if sense:
                                exon_line[4] = str(self.gene_sequences_dict[gene]['exon_seq'][exon + n_unspliced][1])
                            else:
                                exon_line[3] = str(self.gene_sequences_dict[gene]['exon_seq'][exon + n_unspliced][0])
                    except IndexError:  # End of ID reached
                        pass

                    exon_lines.append('\t'.join(exon_line))

                if not self.gene_sequences_dict[gene]['strand_sense']:  # Negative strand sense need reversed order.
                    exon_lines.reverse()
                self.gtf_lines.extend(exon_lines)

    def sort_gtf_lines(self) -> None:

        """ Sorts the gtf lines by the position of the genes (increasing) and returns it."""

        # Builds and uses a dictionary with the start of the gene as key, and all lines related to this gene as value:
        # {start_gene(int): [[gene_line],[transcript_line],[exon_line1],[exon_line2],...]}

        gene_lines_dict = {}
        gene_start = 0  # Validation: This key should remain unused, as every gtf file starts with a gene.
        for index, line in enumerate(self.gtf_lines):
            line_content = line.split('\t')
            if line_content[2] == 'gene':  # This is the next gene line. Initializes dictionary entry.
                gene_start = line_content[3]  # Extract the  key = start of gene.
                gene_lines_dict[gene_start] = []
            gene_lines_dict[gene_start].append(line)  # Append all lines related to this gene.

        sorted_keys = sorted(gene_lines_dict)  # Sorts the keys by their values.
        sorted_gtf_lines = []
        for key in sorted_keys:
            sorted_gtf_lines.extend(gene_lines_dict[key])  # Creates a new list of the gtf lines in the correct order.

        self.gtf_lines = sorted_gtf_lines

    def write_gtf(self,
                  output_coords: str
                  ) -> None:

        """ Writes a gtf file with the information about the differently spliced transcripts.

        Args:
            output_coords(str): Path and name of the output gtf file with the information of all relevant transcripts.
        """
        with open(output_coords, 'w') as gtf_file:
            gtf_file.writelines(self.gtf_lines)


def main():
    """ Main Function."""
    # Inputs
    # Strand with + sense (long)
    # gene_count = 'gene_count/Lypla1_1.csv'
    # gene_count = 'gene_count/Lypla1_5.csv'

    # Strand with + sense (short)
    # gene_count = 'gene_count/1700034P13Rik_5.csv'

    # Strand with - sense.
    # gene_count = 'gene_count/Rp1_1.csv'
    gene_count = 'gene_count/Rik_5_Rp1_5_title.csv'
    # gene_count = 'gene_count/Lypla1_Rp1_100.csv'

    # coordinates_genes = 'gtf/1_coordinates.txt'
    # coordinates_genes = 'gtf/RP1_RIK.gtf'
    coordinates_genes = 'gtf/RP1_RIK_dummy.gtf'

    # p_intron = 0.9997  # Only 1 unspliced intron with random.seed(10)
    p_intron = 0.3

    # Output paths and names.
    name_csv_output = 'Outputs/csv_new_dummy.csv'
    name_gtf_output = 'Outputs/gtf_new_dummy.txt'

    random.seed(10)  # Initializes seed for random functions.

    bts = BuildTranscriptStructure(gene_count, coordinates_genes, p_intron)
    bts.csv_2_dict()  # Generates dictionary from gene count csv file.
    bts.gtf_2_dict()  # Generates dictionary from gtf file.
    bts.make_new_transcripts()  # Generates the differently spliced transcripts.
    bts.make_gtf_info()  # Builds the gtf file of all newly created transcripts.
    bts.sort_gtf_lines()  # Sorts the gtf file by gene occurrence in sequence.
    bts.write_gtf(name_gtf_output)
    bts.write_csv(name_csv_output)


if __name__ == '__main__':
    main()
    print('process completed')
